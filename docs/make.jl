cd(@__DIR__)
using Pkg;
Pkg.activate(@__DIR__);
Pkg.instantiate();
push!(LOAD_PATH, "../")
using SBOL3
using Documenter

DocMeta.setdocmeta!(SBOL3, :DocTestSetup, :(using SBOL3); recursive=true)

makedocs(;
    modules=[SBOL3],
    authors="Soc Virnyl S. Estela <socvirnyl.estela@gmail.com> and contributors",
    repo="https://codeberg.org/uncomfyhalomacro/SBOL3.jl/_edit/{commit}/{path}",
    sitename="SBOL3.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") in ["woodpecker", "drone"],
        canonical="https://uncomfyhalomacro.codeberg.page/SBOL3.jl",
        assets=String[]
    ),
    pages=[
        "Home" => "index.md",
    ],
    doctest=true
)

deploydocs(; repo="codeberg.org/uncomfyhalomacro/SBOL3.jl.git", devbranch="main", target="build", branch="pages", forcepush=true)
