#!/bin/sh

julia --project=@tools make.jl
cp -rv build dev
tar czvf dev.tar.gz dev
rm -rfv dev
