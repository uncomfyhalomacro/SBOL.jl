module SBOL3

using Base: StringVector
struct Collection
    displayId::String
    hasNameSpace::URI
    member::Vector{SBOLTypes}
end

struct Component
    description::String
    displayId::String
    # TODO: Create a separate type for each `name`, `role`, `type`
    name::String
    role::String
    type::String
end

end
