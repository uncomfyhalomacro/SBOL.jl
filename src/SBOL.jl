# SPDX-License-Identifier: MIT

module SBOL

using Parameters
using URIs: URI

"""
  rdfType

Abstract supertype of all SBOL3 types.
"""
abstract type rdfType end

abstract type TopLevel <: rdfType end
abstract type Collection <: rdfType end
abstract type Component <: rdfType end
abstract type Identified <: rdfType end
abstract type Sequence <: rdfType end
abstract type Feature <: rdfType end
abstract type SubComponent <: Feature end
abstract type ComponentReference <: Feature end
abstract type LocalSubComponent <: Feature end

end
